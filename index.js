//imports 
const express= require('express');
const firebase=require('firebase');
const Joi=require('Joi');
const request = require('request');
var GeoPoint = require('geopoint');
var distance = require('google-distance');
distance.apiKey = 'AIzaSyCtZiA9lCU8eplyZzwk04RZ6ax7PJ9OM1o';
// const Nexmo = require('nexmo');
// const nexmo = new Nexmo({
//   apiKey: '00fee92c',
//   apiSecret: '6OGMDnOu3wmqT5di'
// });
// const accountSid = 'AC1e668f67afc3adc064dbf2a77f74fb3b';
// const authToken = 'd82d96e7e4bd8961a37cd6a2b1976d6d';
// const client = require('twilio')(accountSid, authToken);
//using express
const app=express();

//configure firebase 
const DB=firebase.initializeApp({
    apiKey: "AIzaSyAJS8o8jJJnJKlqb5AOQ8py0HObUieReoE",
    authDomain: "test-48740.firebaseapp.com",
    databaseURL: "https://test-48740.firebaseio.com",
    projectId: "test-48740",
    storageBucket: "test-48740.appspot.com",
    messagingSenderId: "27832859549"
});
//to parse the body of the request object
app.use(express.json());

//following is the patient signUp
app.post('/api/signup',(req,res)=>{
    //making a json object 
    const user={
    name:req.body.name,
    phone:req.body.phone,
    email:req.body.email,
    relative1:req.body.relative1,
    relative2:req.body.relative2,
    };

    //making schema for validation
    const schema ={
        name:Joi.string().min(3).required(), //string,minimum length=3,required
        phone:Joi.string().required(),//string,0-9 characters,length range 11-12,required
        email:Joi.string().email().required(), //string,email,required
        relative1:Joi.string().trim().regex(/^[0-9]{11,12}$/),
        relative2:Joi.string().trim().regex(/^[0-9]{11,12}$/),
        password:Joi.string().min(8).required(),
        confirmPassword:Joi.string().valid(Joi.ref('password')).required()//confirmPassword=password
    };

    //doing validation 
    const result=Joi.validate(req.body,schema);
    if(result.error)
    {
        res.status(400).send(result.error);
        return ;
    }
    //get auth Object from firebase for athentication
    const auth=DB.auth();
    
    //
    auth.createUserWithEmailAndPassword(req.body.email,req.body.password)
            .then(()=>{
                //if user is created successfully then following code will insert his profile in users array
                const insertDb=DB.database();
                insertDb.ref().child('users').push(user);
                insertDb.ref().child('userType').push({email:user.email,type:'user'});
                res.status(200).json({status:'done'});
            });
});

//sign in 
app.post('/api/signin',(req,res)=>{
    
    const auth=DB.auth();
    auth.signInWithEmailAndPassword(req.body.email,req.body.password)
        .then(()=>{
            const database=DB.database();
            var userType;
            database.ref('userType').once('value',function(snapshot){
                                 
                snapshot.forEach((obj)=>{
                    if(req.body.email==obj.val().email){
                        userType=obj.val().type;
                        console.log(userType);
                        
                    }
                });
                res.status(200).json({type:userType});
                });
            
            //res.status(200).send(userType);
    })
        .catch(()=> {res.status(400).json({type:false});});
});
app.post('/api/signout',(req,res)=>{
    const auth=DB.auth();
    auth.signOut()
        .then(()=>{res.send('signout')})
        .catch(()=>{res.send('not signout')});
        

});
app.post('/api/driverRegister',(req,res)=>{
    //making a json object 
    const user={
        name:req.body.name,
        phone:req.body.phone,
        email:req.body.email,
        address:req.body.address,
        license:req.body.license
        };
    
        //making schema for validation
        const schema ={
            name:Joi.string().min(3).required(), //string,minimum length=3,required
            phone:Joi.string().trim().regex(/^[0-9]{11,12}$/).required(),//string,0-9 characters,length range 11-12,required
            email:Joi.string().email().required(), //string,email,required
            address:Joi.string().required(),
            license:Joi.string().required(),
            password:Joi.string().min(8).required(),
            confirmPassword:Joi.string().valid(Joi.ref('password')).required()//confirmPassword=password
        };
    
        //doing validation 
        const result=Joi.validate(req.body,schema);
        if(result.error)
        {
            res.status(400).send(result.error);
            return ;
        }
        //get auth Object from firebase for athentication
        const auth=DB.auth();
        
        //
        auth.createUserWithEmailAndPassword(req.body.email,req.body.password)
                .then(()=>{
                    //if user is created successfully then following code will insert his profile in users array
                    const insertDb=DB.database();
                    insertDb.ref().child('drivers').push(user);
                    insertDb.ref().child('driversWithoutAmb').push({email:user.email});
                    insertDb.ref().child('userType').push({email:user.email,type:'driver'});
                    res.status(200).send('done');
                });
});
app.post('/api/ambRegister',(req,res)=>{
    //making a json object 
    const ambulance={
        ownerName:req.body.ownerName,
        phone:req.body.phone,
        address:req.body.address,
        regNo:req.body.regNo
        };
    
        //making schema for validation
        const schema ={
            ownerName:Joi.string().min(3).required(), //string,minimum length=3,required
            phone:Joi.string().trim().regex(/^[0-9]{11,12}$/).required(),//string,0-9 characters,length range 11-12,required
            address:Joi.string().required(),
            regNo:Joi.string().required()
        };
        //doing validation 
        const result=Joi.validate(req.body,schema);
        if(result.error)
        {
            res.status(400).send(result.error);
            return ;
        }
        
            //if user is created successfully then following code will insert his profile in users array
            const insertDb=DB.database();
            insertDb.ref().child('ambulance').push(ambulance);
            insertDb.ref().child('ambulanceWithoutDrivers').push({regno:ambulance.regNo});
            res.status(200).send('done');
        });
app.post('/api/online/',(req,res)=>{
     const schema ={
                 email:Joi.string().email().required(),
                 lati:Joi.required(),
                 longi:Joi.required()
                 };
                 const j={
                 email:req.body.id,
                 lati:req.body.lati,
                 longi:req.body.longi
                 };
                //doing validation 
                const result=Joi.validate(j,schema);
                if(result.error)
                {
                    res.status(400).send(result.error);
                    return ;
                }
    
            if(isOnline(req.body.id)){
                res.json({status:'true'});
                console.log('already onlilne');
            }
            else{ 
                var insertDB=DB.database();
                insertDB.ref().child('online').push(j)
                    .then(()=>{
                        console.log('added to db');
                        res.json({status:'true'});
                    })
                    .catch(()=>{
                        res.json({status:'false'});
                        console.log('wrong');
                    });
                    console.log('went online block');
             }
});
function isOnline(email){
    const database=DB.database();
    var flag=0; 
    database.ref('online').on('value',function(snapshot){
                                 
    snapshot.forEach((obj)=>{
        if(email==obj.val().email){
            flag=obj.key;
            console.log(obj.key);
        }
    });
    });
    return flag;
};
app.get('/api/offline/:id',(req,res)=>{
    offline(req.params.id,res);
});
function offline(id,res){
    const schema ={
        email:Joi.string().email().required()
        };
        const j={
        email:id
        };
       //doing validation 
       const result=Joi.validate(j,schema);
       if(result.error)
       {
           console.log(result.error);
           console.log('zzzzzzzzzzzzzzzzzzzzzzzzz' + id);
           res.status(400).send(result.error);
           return ;
       }
    var key=isOnline(id);
   if(key){
    console.log('key is ' + key);
    var insertDB=DB.database();
       insertDB.ref('online').child(key).remove()
           .then(()=>{
               console.log('went offline');
               res.send('went offline');
           })
           .catch(()=>{
               res.send('something is wrong');
               console.log('wrong');
           });
    console.log(key);
   }
   else{ 
       console.log('already offline');
       res.send('already offline');
    }
}
app.get('/api/test',(req,res)=>{
    const database=DB.database();
    // database.ref('userType').once('value',function(snapshot){
    //     console.log('TTTTTTTTTTTTTTTTTTTTTTTT'+ snapshot.val().type);
    //     snapshot.forEach((obj)=>{
    //             userType=obj.val().type;
    //             console.log(userType);
    //     });
        
    //     });
        database.ref('emergency').child('jj@gmail,com').on('value',function(snapshot){
            var data=snapshot.val();
            console.log(data);
    
            });
    res.send('sdfsdf');


});

app.get('/api/test2',(req,res)=>{
    const database =DB.database();
    //database.ref().child('test3').child('email@gmail,com').push({name:'sfsdf'});
    database.ref('emergency').child('jj@gmail,com').on('value',function(snapshot){
        var data=snapshot.val();
        console.log(data);

        });
   res.send('done');

});
app.get('/api/test3',(req,res)=>{
    var email='em.ail@gmail.com';
    email=email.replace(/\./gi,',');
    console.log(email);
   res.send('done');

});
_EXTERNAL_URL = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=31.544870,74.300390&destinations=31.546006,74.302050&key=AIzaSyCtZiA9lCU8eplyZzwk04RZ6ax7PJ9OM1o';

const callExternalApiUsingRequest = (callback) => {
    request(_EXTERNAL_URL, { json: true }, (err, res, body) => {
    if (err) { 
        return callback(err);
     }
    return callback(body);
    });
}
app.get('/api/ambsearch/:id',(req,res)=>{
    const database=DB.database();
    database.ref('ambulance').on('value',function(snapshot){
    var amb={};
    snapshot.forEach((obj)=>{
        if(req.params.id==obj.val().regNo){
            amb=obj.val();
        }
    });
    res.send(amb);
    });
});
app.get('/api/selectamb/:regno/:email',(req,res)=>{

    
    var driver=0;
    var ambulance=0;
    const database=DB.database();
    database.ref('driversWithoutAmb').once('value',function(snapshot){
    snapshot.forEach((obj)=>{
        if(req.params.email==obj.val().email){
            driver=obj.key;
            console.log('driver 1');
            //database.ref('driversWithoutAmb').child(obj.key).remove()
            // .then(()=>{
            //     console.log('driver deleted');
            // })
            // .catch(()=>{
            //     console.log('not driver deleted');
            // });
        }
    });
    database.ref('ambulanceWithoutDrivers').once('value',function(snapshot){
        snapshot.forEach((obj)=>{
            if(req.params.regno==obj.val().regno){
                ambulance=obj.key;
                console.log('ambulance 1');
                // database.ref('ambulanceWithoutDrivers').child(obj.key).remove()
                // .then(()=>{
                //     console.log('ambulance deleted');
                // })
                // .catch(()=>{
                //     console.log('not ambulance deleted');
                // });

            }
            
        });
        if(driver && ambulance)
        {
            database.ref('driversWithoutAmb').child(driver).remove()
            // .then(()=>{
            //     console.log('driver deleted');
            // })
            // .catch(()=>{
            //     console.log('not driver deleted');
            // });
            database.ref('ambulanceWithoutDrivers').child(ambulance).remove()
                // .then(()=>{
                //     console.log('ambulance deleted');
                // })
                // .catch(()=>{
                //     console.log('not ambulance deleted');
                // });
            database.ref('ambulanceWithDrivers').push({email:req.params.email,regno:req.params.regno});
            console.log('correct');
            res.send('correct');
        }
        else{
            console.log('not correct');
            res.send('not correct');
        }
        });
        
    });
    
    
    
    // database.ref('ambulanceWithDrivers').push({email:req.params.email,regno:req.params.regno});
    //res.send('done');
    //......................
    
});
app.get('/api/emergency/:lati/:long/:email',(req,res)=>{

    emergency(res,req.params.lati,req.params.long,req.params.email);
    

    //............................
    
    
    //res.send(`emergency ${req.params.lati} and ${req.params.long}`);
});
app.post('/api/emergency/',(req,res)=>{

    emergency(res,req.body.lati,req.body.long,req.body.email);
    

    //............................
    
    
    //res.send(`emergency ${req.params.lati} and ${req.params.long}`);
});
async function emergency(res,lati,long,email){

    var e=await findNearestHelp(parseFloat(lati),parseFloat(long));
    if(e==null)
    {
        res.json({emergency:'failed'});
        return;
    }
    e=e.replace(/\./g,',');
    const database =DB.database();
    database.ref('emergency').child(e).child('userInNeed').set(email);
    database.ref('emergency').child(e).child('lati').set(lati);
    database.ref('emergency').child(e).child('long').set(long);
    res.json({emergency:'done'});
}
app.get('/api/emergencyresponded/:driverEmail',(req,res)=>{
    const database =DB.database();
    var driverEmail=req.params.driverEmail;
    driverEmail=driverEmail.replace(/\./g,',');
    database.ref('emergency').child(driverEmail).once('value',function(snapshot){
        var data=snapshot.val();
         //database.ref('emergencyRespondedDriver').child(driverEmail).push(data);
        database.ref('emergencyRespondedDriver').child(driverEmail).child('lati').set(data.lati);
        database.ref('emergencyRespondedDriver').child(driverEmail).child('long').set(data.long);
        database.ref('emergencyRespondedDriver').child(driverEmail).child('userInNeed').set(data.userInNeed);
        var patientEmail=data.userInNeed;
        patientEmail=patientEmail.replace(/\./g,',');
        database.ref('emergencyRespondedPatient').child(patientEmail).child('driver').set(req.params.driverEmail);
        database.ref('online').once('value',function(snapshot){
            snapshot.forEach((obj)=>{
                if(obj.val().email==req.params.driverEmail){
                    database.ref('emergencyRespondedPatient').child(patientEmail).child('lati').set(obj.val().lati);
                    database.ref('emergencyRespondedPatient').child(patientEmail).child('longi').set(obj.val().longi);
                }
            });
        });
        database.ref('emergency').child(driverEmail).remove();
       
    });
    //database.ref('emergency').child(driverEmail).remove();
    res.json({status:'resoponed'});
});
app.post('/api/emergencyresponded/',(req,res)=>{
    const database =DB.database();
    var driverEmail=req.body.driverEmail;
    driverEmail=driverEmail.replace(/\./g,',');
    database.ref('emergency').child(driverEmail).once('value',function(snapshot){
        var data=snapshot.val();
         //database.ref('emergencyRespondedDriver').child(driverEmail).push(data);
        database.ref('emergencyRespondedDriver').child(driverEmail).child('lati').set(data.lati);
        database.ref('emergencyRespondedDriver').child(driverEmail).child('long').set(data.long);
        database.ref('emergencyRespondedDriver').child(driverEmail).child('userInNeed').set(data.userInNeed);
        var patientEmail=data.userInNeed;
        patientEmail=patientEmail.replace(/\./g,',');
        database.ref('emergencyRespondedPatient').child(patientEmail).child('driver').set(req.body.driverEmail);
        database.ref('online').once('value',function(snapshot){
            snapshot.forEach((obj)=>{
                if(obj.val().email==req.body.driverEmail){
                    database.ref('emergencyRespondedPatient').child(patientEmail).child('lati').set(obj.val().lati);
                    database.ref('emergencyRespondedPatient').child(patientEmail).child('longi').set(obj.val().longi);
                }
            });
        });
        database.ref('emergency').child(driverEmail).remove();
       
    });
    //database.ref('emergency').child(driverEmail).remove();
    res.json({status:'true'});
});
app.get('/api/emergencynotresponded/:driverEmail',(req,res)=>{
    const database=DB.database();
    var driverEmail=req.params.driverEmail;
    driverEmail=driverEmail.replace(/\./g,',');
    var data;
    database.ref('emergency').child(driverEmail).once('value',function(snapshot){
        data=snapshot.val();
        console.log(data);
        database.ref('emergency').child(driverEmail).remove();
        emergency(res,data.lati,data.long,data.userInNeed);
        });
   
    offline(req.params.driverEmail,res);
    
    res.json({status:'not responded'});
});
app.post('/api/emergencynotresponded/',(req,res)=>{
    const database=DB.database();
    var driverEmail=req.body.driverEmail;
    driverEmail=driverEmail.replace(/\./g,',');
    var data;
    database.ref('emergency').child(driverEmail).once('value',function(snapshot){
        data=snapshot.val();
        console.log(data);
        database.ref('emergency').child(driverEmail).remove();
        emergency(res,data.lati,data.long,data.userInNeed);
        });
   
    offline(req.body.driverEmail,res);
    
    res.json({status:'true'});
});
app.get('/api/informrelative/:id',(req,res)=>{
    const database=DB.database();
     database.ref('users').once('value',function(snapshot){
        snapshot.forEach((obj)=>{
            if(obj.val().email==req.params.id){
                res.json({r1:obj.val().relative1,r2:obj.val().relative2});
                return;
            }
        });
    });

    
});
app.post('/api/informrelative/',(req,res)=>{
    const database=DB.database();
     database.ref('users').once('value',function(snapshot){
        snapshot.forEach((obj)=>{
            if(obj.val().email==req.body.id){
                res.json({r1:obj.val().relative1,r2:obj.val().relative2});
                return;
            }
        });
    });

    
});
app.post('/api/requestblood/',(req,res)=>{
    const database=DB.database();
    var phone;
    database.ref('users').once('value',function(snapshot){
        snapshot.forEach((obj)=>{
            if(obj.val().email==req.body.id){
                phone=obj.val().phone;
                database.ref('bloodRequest').push({bloodGroup:req.body.g,email:req.body.id,phone:phone});
            }
        });
    });

    res.json({status:"true"});
});
app.get('/api/requestblood/:g/:id',(req,res)=>{
    const database=DB.database();
    var phone;
    database.ref('users').once('value',function(snapshot){
        snapshot.forEach((obj)=>{
            if(obj.val().email==req.params.id){
                phone=obj.val().phone;
                database.ref('bloodRequest').push({bloodGroup:req.params.g,email:req.params.id,phone:phone});
            }
        });
    });

    res.json({status:"true"});
});
app.post('/api/acceptblood/',(req,res)=>{
    const database=DB.database();
    var phone;
    database.ref('bloodRequest').once('value',function(snapshot){
        snapshot.forEach((obj)=>{
            if(obj.val().email==req.body.email){
                database.ref('bloodDonated').push({bloodGroup:obj.val().bloodGroup,email:obj.val().email,phone:obj.val().phone,accepterEmail:req.body.id});
                database.ref('bloodRequest').child(obj.key).remove();
            }
        });
    });
    res.json({status:"true"});
});
app.get('/api/acceptblood/:email/:id',(req,res)=>{
    const database=DB.database();
    database.ref('bloodRequest').once('value',function(snapshot){
        snapshot.forEach((obj)=>{
            if(obj.val().email==req.params.email){
                database.ref('bloodDonated').push({bloodGroup:obj.val().bloodGroup,email:obj.val().email,phone:obj.val().phone,accepterEmail:req.params.id});
                database.ref('bloodRequest').child(obj.key).remove();
            }
        });
    });
    res.json({status:"true"});
});
app.get('/api/blood',(req,res)=>{
    const database=DB.database();
    var o={};
    var key='abc';
    o[key]=[];
    database.ref('bloodRequest').once('value',function(snapshot){
        snapshot.forEach((obj)=>{
            var data={
                bloodgroup:obj.val().bloodGroup,
                email:obj.val().email,
                phone:obj.val().phone
            };
            o[key].push(data);
        });
        console.log('hereee');
        res.send(o);
    });
});
app.post('/api/blooddata',(req,res)=>{
    const database=DB.database();
    var o={};
    var key='abc';
    o[key]=[];
    database.ref('bloodRequest').once('value',function(snapshot){
        snapshot.forEach((obj)=>{
            var data={
                bloodgroup:obj.val().bloodGroup,
                email:obj.val().email,
                phone:obj.val().phone
            };
            o[key].push(data);
        });
        console.log('here');
        
        res.json(o);
    });
});
app.get('/api/acceptnotification',(req,res)=>{
    res.send('accept notification');
});
app.get('/api/seeresponse',(req,res)=>{
    res.send('see response');mm
});
app.get('/api/getprofile/:id',(req,res)=>{
    res.send(`get profile ${req.params.id}`);
});

function findNearestHelp(lati,long)
{
    return new Promise((resolve,reject)=>{
        var distance=10000;
        var driverEmail;
        const database=DB.database();
        database.ref('online').once('value',function(snapshot){
        snapshot.forEach((obj)=>{
            var aLati=parseFloat(obj.val().lati);
            var aLong=parseFloat(obj.val().longi);
            point1 = new GeoPoint(aLati,aLong);
            point2 = new GeoPoint(lati, long);
            var dis = point1.distanceTo(point2, true)//output in kilometers
            if(dis<distance){
                distance=dis;
                driverEmail=obj.val().email;
                //console.log('hhhhhhhhhhhh'+driverEmail);
            }
        });
        //console.log('zzzzzzzzzzzzzzzzzzhhhhhh'+driverEmail);
        resolve (driverEmail);
        });
    });
    
    
    
}

//console.log('i love you');         
const port=process.env.PORT || 3000;
app.listen(3000,()=>{console.log(`listening on port ${port}...`)});